import mockingoose from "mockingoose";
import request from "supertest";
import { Model, Query, Types } from "mongoose";
import dotenv from "dotenv";
import { Server } from "http";

dotenv.config();

import { UserExport } from "../src/model/user";
import { DatabaseManager } from "../src/db/databaseManager";
import { UserService } from '../src/services/userService';
import { ApiRequest } from "../src/model/apiRequest";
import { ApiMessage } from "../src/model/apiMessages/apiMessage";
import { ApiSuccess } from "../src/model/apiMessages/apiSuccess";
import { ApiError } from "../src/model/apiMessages/apiError";

const dbm: Promise<DatabaseManager> = DatabaseManager.getDb()
let userModel: Model<UserExport>;

const userTemplate: UserExport[] = [
    {
        _id: "507f191e810c19729de860ea",
        pseudo: "toto",
        email: "toto@etu.uca.fr",
        posts: []
    },
    {
        _id: "507f191e810c19729df860ea",
        pseudo: "totoff",
        email: "totodf@etu.uca.fr",
        posts: []
    }
] as unknown as UserExport[];

describe("Test User Service / Router", () => {
    /** Server to send request */
    let server: Server;
    /** Token of the current user, used when we want to make correct requests */
    let token: string;
    /** Wrong token for testing, used when we want to modify wrong data */
    let wrongToken: string;

    afterAll(()=> {
        server.close();
    })

    beforeAll(async () => {
        userModel = (await dbm).getModel<UserExport>("users");
        server = require("../src/server");
        mockingoose.resetAll();
        mockingoose(userModel)
        .toReturn(
            (query: Query<UserExport>) => {
                if (!query.getQuery()._id || query.getQuery()._id === "bb7f191e810c19729de860ea" || query.getQuery()._id === new Types.ObjectId("bb7f191e810c19729de860ea")) {
                    if (!query.getQuery().email || query.getQuery().email === "myTest@test.test")
                    return {
                        _id: "bb7f191e810c19729de860ea",
                        email: "myTest@test.test",
                        pseudo: "myTest",
                        posts: [],
                        hash: "3bf4ea1d0846df1a52fd6123e7b7dd412267831faa8eb01e3633625c33a6746d263ecac8ba50e98ced24eefce3604f096f7c578275999e2dbdfef6487794a541b53837b05918faec63d6f3795a1e92c86e027767ce81efd1a45c932fa36bc8feef12ada06ddebc3fdbdfa268220143c04bf365ce227ba45d1a72829f9d3c39bc730c4108c24fe572b98e6a995bac714ed6ae042e5a3ab34b3fa06631b7397482611ce8c52fd103268730094c615fb3f44a14f2e55bad150d2131c62dbbe22dee8189f81b3fd696fade0cb6002bf90182136fe71d25c5f6f9f900d0afd8af9c815cff12439e54df1df00fe2e564661f0bfc3ba22f2d7bb4b27fc62b2a0b5dea370a0b7706c8c8468cc3ef9cf325994d00cc67253c42985a8068bad8d51ad8d23e39ee7b1839971ac11c2ba4086a6b4ac9b59291b02662d4e27b8040061f84803294e46441f5164591f40e3889f7ef826f5833a07ff97a55028f90a91760f6233b2608b2849aa4fc76f53bf37f8249c182febcce3bb71d51ef787c11ffa759e38d47e6b74dd303f50ba213f88f920e8c26585e0b3e40646cc3922474dd2b4d5d7d7e68cf60ddec8dffe14d95035ddc5b37a4403f45f0d270db4f7db5c4f66484d54078558be2f82caa0063d92dc49e5460df727144253a5075980c8bc6dfcc4ec19144bc33245aafe3fe81df6afd4392c06e0e0712cbde6984405708da798ab109",
                        salt: "0529699275c3fb50a92dc896656ff93f"
                    };
                }
            }, "findOne")
        .toReturn(
            [
                {
                    _id: "bb7f191e810c19729de345ea",
                    email: "fake@fake.fake",
                    pseudo: "fake",
                    posts: []
                },
                {
                    _id: "bb7f191e810c19729de860ea",
                    email: "myTest@test.test",
                    pseudo: "myTest",
                    posts: []
                }
            ], "find")
        .toReturn((query: Query<UserExport>): number => { return query.getQuery().email === "myTest@test.test" ? 1 : 0}, "countDocuments")
        .toReturn(userTemplate);
        wrongToken = (await userModel.find())[1].generateJWT();
    });

    test("test insertUser", async () => {
        let res: ApiMessage = await UserService.insertUser({
            body: {
            email: "totoInsert@gmail.com",
            password: "fakeAccount"
        }} as unknown as ApiRequest).catch(data => data);
        expect(res).toBeInstanceOf(ApiSuccess);
        const success: ApiSuccess = res as ApiSuccess;
        expect(success.code).toBe(201);
        expect(success.response).toHaveProperty("token");
        const res2 = await UserService.insertUser({body: { password: "fakeAccount" } } as unknown as ApiRequest).catch(data => data);
        const res3 = await UserService.insertUser({ body: { email: "totoInsert@gmail.com" } } as unknown as ApiRequest).catch(data => data);
        const res4 = await UserService.insertUser(
            { body: { 
                email: "totoInsertgmail.com",
                password: "fakeAccount"
            }} as unknown as ApiRequest).catch(data => data);
        expect(res2).toBeInstanceOf(ApiError);
        expect(res3).toBeInstanceOf(ApiError);
        expect(res4).toBeInstanceOf(ApiError);
        expect(res2.code).toBe(422);
        expect(res3.code).toBe(422);
        expect(res4.code).toBe(422);

        /** Email adress already taken */
        await request(server).post('/api/v1/users/').send({
            email: "myTest@test.test",
            password: "blblbl"
        }).then((response: request.Response) => {
            expect(response.status).toBe(422);
        })

        /** OK */
        await request(server).post('/api/v1/users/').send({
            email: "perfectEmailAdress@test.test",
            password: "blblbl"
        }).then((response: request.Response) => {
            expect(response.status).toBe(201);
            expect(response.body).toHaveProperty("token");
        })
    }, 10000);

    test("test log user in", async () => {
        /** OK */
        const res = await request(server).post('/api/v1/users/login').send({
            email: "myTest@test.test",
            password: "root"
        }).then((response: request.Response) => {
            expect(response.status).toBe(200);
            expect(response.body).toHaveProperty("token");
            token = response.body.token;
        });

        /** incorrect password */
        await request(server).post('/api/v1/users/login').send({
            email: "myTest@test.test",
            password: "wrongPassword"
        }).then((response: request.Response) => {
            expect(response.status).toBe(400);
        });

        /** email not existing */
        await request(server).post('/api/v1/users/login').send({
            email: "unexistingMailAdresse@gmail.com",
            password: "wrongPassword"
        }).then((response: request.Response) => {
            expect(response.status).toBe(400);
        });
    }, 10000);

    test("test (un)authorized request", async () => {
        /** Access authorized */
        await request(server)
        .put('/api/v1/users/')
        .send({})
        .then((response: request.Response) => {
            expect(response.status).toBe(401);
        });

        /** Access not authorized */
        await request(server)
        .put('/api/v1/users/')
        .set('Authorization', 'Token ' + token)
        .send({})
        .then((response: request.Response) => {
            expect(response.status).not.toBe(401);
        });
    }, 10000);

    test("test update user", async () => {
        /** With missing parameters */
        await request(server)
        .put('/api/v1/users/')
        .set('Authorization', 'Token ' + token)
        .send({})
        .then((response: request.Response) => {
            expect(response.status).toBe(422);
        });

        /** OK */
        await request(server)
        .put('/api/v1/users/')
        .set('Authorization', 'Token ' + token)
        .send({
            id: "bb7f191e810c19729de860ea",
            pseudo: "myNewPseudo",
            avatar: "myNewAvatar"
        })
        .then((response: request.Response) => {
            expect(response.status).toBe(200);
        });

        /** With no authorization */
        await request(server)
        .put('/api/v1/users/')
        .set('Authorization', 'Token ' + wrongToken)
        .send({
            id: "bb7f191e810c19729de860ea",
            pseudo: "myNewPseudo"
        })
        .then((response: request.Response) => {
            expect(response.status).toBe(403);
        });
    }, 10000);

    test("test get user", async () => {
        /** OK */
        await request(server)
        .get('/api/v1/users/bb7f191e810c19729de860ea')
        .set('Authorization', 'Token ' + token)
        .send({})
        .then((response: request.Response) => {
            expect(response.status).toBe(200);
            expect(response.body).toMatchObject({
                _id: "bb7f191e810c19729de860ea",
                email: "myTest@test.test",
                pseudo: "myTest"
            })
        });

        /** KO, we still get a body but an empty one */
        await request(server)
        .get('/api/v1/users/aa7f191e810c14359de860ea')
        .set('Authorization', 'Token ' + token)
        .send({})
        .then((response: request.Response) => {
            expect(response.status).toBe(200);            
        });
    }, 10000);

    test("test delete user", async () => {
        /** OK */
        await request(server)
        .delete('/api/v1/users/bb7f191e810c19729de860ea')
        .set('Authorization', 'Token ' + token)
        .then((response: request.Response) => {
            expect(response.status).toBe(200);
        });

        /** With no authorization */
        await request(server)
        .delete('/api/v1/users/bb7f191e810c19729de860ea')
        .set('Authorization', 'Token ' + wrongToken)
        .then((response: request.Response) => {
            expect(response.status).toBe(403);
        });
    }, 10000);

    test("test get post user", async () => {
        /** With wrong ID. Format should be an object ID */
        await request(server)
        .get('/api/v1/users/bb7f/posts?limit=2&start=1')
        .set('Authorization', 'Token ' + token)
        .then((response: request.Response) => {
            expect(response.status).toBe(400);
        });

        /** Get post without limit and start parameters */
        await request(server)
        .get('/api/v1/users/bb7f191e810c19729de860ea/posts')
        .set('Authorization', 'Token ' + token)
        .then((response: request.Response) => {
            expect(response.status).toBe(422);
        });

        /** Get post for an user who is not existing */
        await request(server)
        .get('/api/v1/users/bb7f191e810c19729de060ea/posts?limit=2&start=1')
        .set('Authorization', 'Token ' + token)
        .then((response: request.Response) => {
            expect(response.status).toBe(400);
        });

        /** OK */
        await request(server)
        .get('/api/v1/users/bb7f191e810c19729de860ea/posts?limit=2&start=1')
        .set('Authorization', 'Token ' + token)
        .then((response: request.Response) => {
            expect(response.status).toBe(200);
        });
    }, 10000);
});
