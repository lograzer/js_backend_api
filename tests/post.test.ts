import mockingoose from "mockingoose";
import request from "supertest";
import { Model, Query, Types } from "mongoose";
import dotenv from "dotenv";
import { Server } from "http";

dotenv.config();

import { PostExport, PostCandidate } from "../src/model/post";
import { DatabaseManager } from "../src/db/databaseManager";
import { UserExport } from "../src/model/user";
import { deletePicture } from "../src/services/pictureService";



const dbm: Promise<DatabaseManager> = DatabaseManager.getDb()
let postModel: Model<PostExport>;
let userModel: Model<UserExport>;

describe("Test Post Service / Router / Images", () => {
    /** Server to send request */
    let server: Server;
    /** Token of the current user, used when we want to make correct requests */
    let token: string;
    /** Wrong token for testing, used when we want to modify wrong data */
    let wrongToken: string; 

    afterAll(async ()=> {
        server.close();
    })

    beforeAll(async () => {
        postModel = (await dbm).getModel<PostExport>("posts");
        userModel = (await dbm).getModel<UserExport>("users");
        server = require("../src/server");
        mockingoose.resetAll();

        /** PostModel Mock */
        mockingoose(postModel)
        .toReturn([
            {
                _id: "5e5d4c29578dd6051857093f",
                title: "post1",
                description: "description post 1",
                picture: "myPicture.png",
                author: {
                    _id: "bb7f191e810c19729de860ea",
                    avatar: null,
                    pseudo: "myTest"
                },
                comments: [],
                likes: []
            },
            {
                _id: "5e5d4c29578ddae51857093f",
                title: "post2",
                description: "description post 2",
                picture: "myPicture.png",
                author: {
                    _id: "bb7f191e810c19729de860ea",
                    avatar: null,
                    pseudo: "myTest"
                },
                comments: [],
                likes: []
            }
        ] as unknown as PostExport[], "find")
        .toReturn((query: Query<PostExport>) => {
            if (query.getQuery()._id === "5e5d4c29578dd6051857093f" || query.getQuery()._id === new Types.ObjectId("5e5d4c29578dd6051857093f")) {
                return {
                    _id: new Types.ObjectId("5e5d4c29578dd6051857093f"),
                    title: "post1",
                    description: "description post 1",
                    picture: "myPicture.png",
                    author: {
                        _id: "bb7f191e810c19729de860ea",
                        avatar: null,
                        pseudo: "myTest"
                    },
                    comments: [
                        {
                            _id: new Types.ObjectId("5e5d4c29578dd6051857aaaa"),
                            message: "Top!",
                            author: {
                                _id: "bb7f191e810c19729de860ea",
                                avatar: null,
                                pseudo: "myTest"
                            }
                        },
                        {
                            _id: new Types.ObjectId("5e5d4c29578ddbbb1857abbb"),
                            message: "Oui!",
                            author: {
                                _id: "bb7f191e810c19729de860ea",
                                avatar: null,
                                pseudo: "myTest"
                            }
                        }
                    ],
                    likes: {
                        _id: "bb7f191e810c19729de860ea",
                        avatar: null,
                        pseudo: "myTest"
                    }
                }
            }
        }, "findOne");

        /** UserModel Mock */
        mockingoose(userModel)
        .toReturn(
            (query: Query<UserExport>) => {
                let cond: boolean = false;
                if (typeof query.getQuery()._id === "object") {
                    cond = query.getQuery()._id.toHexString() === "bb7f191e810c19729de860ea";
                }
                if (!query.getQuery()._id || query.getQuery()._id === "bb7f191e810c19729de860ea" || query.getQuery()._id === new Types.ObjectId("bb7f191e810c19729de860ea") || cond) {
                    if (!query.getQuery().email || query.getQuery().email === "myTest@test.test")
                    return {
                        _id: "bb7f191e810c19729de860ea",
                        email: "myTest@test.test",
                        pseudo: "myTest",
                        posts: [new Types.ObjectId("5e5d4c29578dd6051857093f")],
                        hash: "3bf4ea1d0846df1a52fd6123e7b7dd412267831faa8eb01e3633625c33a6746d263ecac8ba50e98ced24eefce3604f096f7c578275999e2dbdfef6487794a541b53837b05918faec63d6f3795a1e92c86e027767ce81efd1a45c932fa36bc8feef12ada06ddebc3fdbdfa268220143c04bf365ce227ba45d1a72829f9d3c39bc730c4108c24fe572b98e6a995bac714ed6ae042e5a3ab34b3fa06631b7397482611ce8c52fd103268730094c615fb3f44a14f2e55bad150d2131c62dbbe22dee8189f81b3fd696fade0cb6002bf90182136fe71d25c5f6f9f900d0afd8af9c815cff12439e54df1df00fe2e564661f0bfc3ba22f2d7bb4b27fc62b2a0b5dea370a0b7706c8c8468cc3ef9cf325994d00cc67253c42985a8068bad8d51ad8d23e39ee7b1839971ac11c2ba4086a6b4ac9b59291b02662d4e27b8040061f84803294e46441f5164591f40e3889f7ef826f5833a07ff97a55028f90a91760f6233b2608b2849aa4fc76f53bf37f8249c182febcce3bb71d51ef787c11ffa759e38d47e6b74dd303f50ba213f88f920e8c26585e0b3e40646cc3922474dd2b4d5d7d7e68cf60ddec8dffe14d95035ddc5b37a4403f45f0d270db4f7db5c4f66484d54078558be2f82caa0063d92dc49e5460df727144253a5075980c8bc6dfcc4ec19144bc33245aafe3fe81df6afd4392c06e0e0712cbde6984405708da798ab109",
                        salt: "0529699275c3fb50a92dc896656ff93f"
                    };
                }
            }, "findOne")
        .toReturn(
            [
                {
                    _id: "bb7f191e810c19729de345ea",
                    email: "fake@fake.fake",
                    pseudo: "fake",
                    posts: []
                },
                {
                    _id: "bb7f191e810c19729de860ea",
                    email: "myTest@test.test",
                    pseudo: "myTest",
                    posts: []
                }
            ], "find")
        .toReturn((query: Query<UserExport>): number => { 
            console.log(query.getQuery());
            if (!query.getQuery()) { 
                return 2;
            }
            return query.getQuery().email === "myTest@test.test" ? 1 : 0;
        }, "countDocuments");

        wrongToken = (await userModel.find())[0].generateJWT();
        /** Get a token to do the testing */
        await request(server).post('/api/v1/users/login').send({
            email: "myTest@test.test",
            password: "root"
        }).then((response: request.Response) => {
            token = response.body.token;
        });
    });

    test("test add a new Post and picture service", async () => {
        const newPost: PostCandidate = {
            title: "newPostToAdd",
            description: "description post to add"
        } as unknown as PostCandidate;

        /** No image */
        await request(server)
        .post('/api/v1/posts/')
        .set('Authorization', 'Token ' + token)
        .send(newPost)
        .then((response: request.Response) => {
            expect(response.status).toBe(422);
        });

        /** No Title */
        await request(server)
        .post('/api/v1/posts/')
        .set('Authorization', 'Token ' + token)
        .attach('picture', `${__dirname}/test.png`)
        .field("description", newPost.description)
        .then((response: request.Response) => {
            expect(response.status).toBe(422);
        });

        /** OK */
        await request(server)
        .post('/api/v1/posts/')
        .set('Authorization', 'Token ' + token)
        .attach('picture', `${__dirname}/test.png`)
        .field("title", newPost.title)
        .field("description", newPost.description)
        .then((response: request.Response) => {
            expect(response.status).toBe(201);
            expect(response.body).toMatchObject(newPost);
            expect(response.body.picture).toBeTruthy;
            deletePicture(response.body.picture);

        });
    }, 10000);

    test("test update an existing Post", async () => {
        const updatedPost = {
            id: new Types.ObjectId("5e5d4c29578dd6051857093f"),
            title: "post1",
            description: "new description"
        };

        /** Missing field */
        await request(server)
        .put('/api/v1/posts')
        .set('Authorization', 'Token ' + token)
        .send({... updatedPost, title: undefined})
        .then((response: request.Response) => {
            expect(response.status).toBe(422);
        });

        /** Wrong id type */
        await request(server)
        .put('/api/v1/posts')
        .set('Authorization', 'Token ' + token)
        .send({... updatedPost, id: "nopnop"})
        .then((response: request.Response) => {
            expect(response.status).toBe(422);
        });

        /** Unexisting post */
        await request(server)
        .put('/api/v1/posts')
        .set('Authorization', 'Token ' + token)
        .send({... updatedPost, id: "aaaaac29578dd6051857093f"})
        .then((response: request.Response) => {
            expect(response.status).toBe(400);
        });

        /** Unauthorized modification */
        await request(server)
        .put('/api/v1/posts')
        .set('Authorization', 'Token ' + wrongToken)
        .send(updatedPost)
        .then((response: request.Response) => {
            expect(response.status).toBe(403);
        });


        /** OK */
        await request(server)
        .put('/api/v1/posts')
        .set('Authorization', 'Token ' + token)
        .send(updatedPost)
        .then((response: request.Response) => {
            expect(response.status).toBe(200);
        });
    }, 10000);

    test("test add a comment", async () => {
        /** Wrong id type  */
        await request(server)
        .post('/api/v1/posts/5e5/comments')
        .set('Authorization', 'Token ' + token)
        .send({ message: "Message test" })
        .then((response: request.Response) => {
            expect(response.status).toBe(422);
        });

        /** No message  */
        await request(server)
        .post('/api/v1/posts/5e5d4c29578dd6051857093f/comments')
        .set('Authorization', 'Token ' + token)
        .send()
        .then((response: request.Response) => {
            expect(response.status).toBe(422);
        });

        /** Post does not exist  */
        await request(server)
        .post('/api/v1/posts/aa5d4c29578dd6051857093f/comments')
        .set('Authorization', 'Token ' + token)
        .send({ message: "Message test" })
        .then((response: request.Response) => {
            expect(response.status).toBe(400);
        });

        /** OK */
        await request(server)
        .post('/api/v1/posts/5e5d4c29578dd6051857093f/comments')
        .set('Authorization', 'Token ' + token)
        .send({ message: "Message test" })
        .then((response: request.Response) => {
            expect(response.status).toBe(200);
        });
    }, 10000);

    test("test get a post and its comments", async () => {
        /** Wrong id type */
        await request(server)
        .get('/api/v1/posts/5e5/comments?limit=2&start=0')
        .set('Authorization', 'Token ' + token)
        .then((response: request.Response) => {
            expect(response.status).toBe(422);
        });

        /** Missing start and limit parameters */
        await request(server)
        .get('/api/v1/posts/5e5d4c29578dd6051857093f/comments')
        .set('Authorization', 'Token ' + token)
        .then((response: request.Response) => {
            expect(response.status).toBe(422);
        });

        /** Wrong start and limit parameters */
        await request(server)
        .get('/api/v1/posts/5e5d4c29578dd6051857093f/comments?limit=rez&start=sd')
        .set('Authorization', 'Token ' + token)
        .then((response: request.Response) => {
            expect(response.status).toBe(422);
        });

        /** Unexisting post */
        await request(server)
        .get('/api/v1/posts/5e5d4c29578dd60518aaaa3f/comments?limit=2&start=0')
        .set('Authorization', 'Token ' + token)
        .then((response: request.Response) => {
            expect(response.status).toBe(400);
        });

        /** OK */
        await request(server)
        .get('/api/v1/posts/5e5d4c29578dd6051857093f/comments?limit=1&start=0')
        .set('Authorization', 'Token ' + token)
        .then((response: request.Response) => {
            expect(response.status).toBe(200);
            expect(response.body).toMatchObject({
                title: "post1",
                description: "description post 1"
            });
        });
    }, 10000);

    test("test delete a comment", async () => {
        /** Wrong comment id type */
        await request(server)
        .delete('/api/v1/posts/5e5d4c29578dd6051857093f/comments/a')
        .set('Authorization', 'Token ' + token)
        .then((response: request.Response) => {
            expect(response.status).toBe(422);
        });

        /** Wrong post id type */
        await request(server)
        .delete('/api/v1/posts/5e/comments/5e5d4c29578dd6051857aaaa')
        .set('Authorization', 'Token ' + token)
        .then((response: request.Response) => {
            expect(response.status).toBe(422);
        });

        /** Unexisting post */
        await request(server)
        .delete('/api/v1/posts/5e5d4c29578dd6aaaaaaaaaa/comments/5e5d4c29578dd6051857aaaa')
        .set('Authorization', 'Token ' + token)
        .then((response: request.Response) => {
            expect(response.status).toBe(400);
        });

        /** Unexisting comment */
        await request(server)
        .delete("/api/v1/posts/5e5d4c29578dd6051857093f/comments/5e5d4aaa578dd6051857aaaa")
        .set('Authorization', 'Token ' + wrongToken)
        .then((response: request.Response) => {
            expect(response.status).toBe(400);
        });
    }, 10000);

    test("test add a like", async () => {
        /** Wrong id type  */
        await request(server)
        .post('/api/v1/posts/5e5/likes')
        .set('Authorization', 'Token ' + token)
        .then((response: request.Response) => {
            expect(response.status).toBe(422);
        });

        /** OK */
        await request(server)
        .post('/api/v1/posts/5e5d4c29578dd6051857093f/likes')
        .set('Authorization', 'Token ' + wrongToken)
        .then((response: request.Response) => {
            expect(response.status).toBe(200);
        });
    }, 10000);

    test("test remove a like", async () => {
        /** Wrong id type  */
        await request(server)
        .delete('/api/v1/posts/5e5/likes')
        .set('Authorization', 'Token ' + token)
        .then((response: request.Response) => {
            expect(response.status).toBe(422);
        });

        /** KO */
        await request(server)
        .delete('/api/v1/posts/5e5d4c29578dd6051857093f/likes')
        .set('Authorization', 'Token ' + wrongToken)
        .then((response: request.Response) => {
            expect(response.status).toBe(400);
        });

    }, 10000);

    test("test get a post and its likes", async () => {
        /** Wrong id type */
        await request(server)
        .get('/api/v1/posts/5e5/likes?limit=2&start=0')
        .set('Authorization', 'Token ' + token)
        .then((response: request.Response) => {
            expect(response.status).toBe(422);
        });

        /** Wrong type start and limit parameters */
        await request(server)
        .get('/api/v1/posts/5e5d4c29578dd6051857093f/likes?limit=er&start=sdf')
        .set('Authorization', 'Token ' + token)
        .then((response: request.Response) => {
            expect(response.status).toBe(422);
        });

        /** Missing start and limit parameters */
        await request(server)
        .get('/api/v1/posts/5e5d4c29578dd6051857093f/likes')
        .set('Authorization', 'Token ' + token)
        .then((response: request.Response) => {
            expect(response.status).toBe(422);
        });

        /** Unexisting post */
        await request(server)
        .get('/api/v1/posts/5e5d4c29578dd60518aaaa3f/likes?limit=2&start=0')
        .set('Authorization', 'Token ' + token)
        .then((response: request.Response) => {
            expect(response.status).toBe(400);
        });

        /** OK */
        await request(server)
        .get('/api/v1/posts/5e5d4c29578dd6051857093f/likes?limit=1&start=0')
        .set('Authorization', 'Token ' + token)
        .then((response: request.Response) => {
            expect(response.status).toBe(200);
            expect(response.body).toMatchObject({
                title: "post1",
                description: "description post 1"
            });
        });
    }, 10000);

    test("test get posts randomly", async () => {
        /** Missing limit parameter */
        await request(server)
        .get('/api/v1/posts/random')
        .set('Authorization', 'Token ' + token)
        .then((response: request.Response) => {
            expect(response.status).toBe(422);
        });

        /** Wrong type limit parameter */
        await request(server)
        .get('/api/v1/posts/random?limit=ezr')
        .set('Authorization', 'Token ' + token)
        .then((response: request.Response) => {
            expect(response.status).toBe(422);
        });

        /** OK */
        await request(server)
        .get('/api/v1/posts/random?limit=2')
        .set('Authorization', 'Token ' + token)
        .then((response: request.Response) => {
            expect(response.status).toBe(200);
        });
    }, 10000);

    test("test get all posts", async () => {
        /** Missing limit and start parameters */
        await request(server)
        .get('/api/v1/posts')
        .set('Authorization', 'Token ' + token)
        .then((response: request.Response) => {
            expect(response.status).toBe(422);
        });

        /** Wrong type start and limit parameters */
        await request(server)
        .get('/api/v1/posts?limit=er&start=sdf')
        .set('Authorization', 'Token ' + token)
        .then((response: request.Response) => {
            expect(response.status).toBe(422);
        });

        /** OK */
        await request(server)
        .get('/api/v1/posts?limit=2&start=0')
        .set('Authorization', 'Token ' + token)
        .then((response: request.Response) => {
            expect(response.status).toBe(200);
        });
    }, 10000);

    test("test delete a post", async () => {
        /** Wrong id type */
        await request(server)
        .delete('/api/v1/posts/5e5')
        .set('Authorization', 'Token ' + token)
        .then((response: request.Response) => {
            expect(response.status).toBe(422);
        });

        /** Post does not exist */
        await request(server)
        .delete('/api/v1/posts/aaaaaaa9578dd6051857093f')
        .set('Authorization', 'Token ' + token)
        .then((response: request.Response) => {
            expect(response.status).toBe(400);
        });

        /** Unauthorized removal */
        await request(server)
        .delete('/api/v1/posts/5e5d4c29578dd6051857093f')
        .set('Authorization', 'Token ' + wrongToken)
        .then((response: request.Response) => {
            expect(response.status).toBe(403);
        });

        /** OK */
        await request(server)
        .delete('/api/v1/posts/5e5d4c29578dd6051857093f')
        .set('Authorization', 'Token ' + token)
        .then((response: request.Response) => {
            expect(response.status).toBe(200);
        });
    }, 10000);

    test("test get a post", async () => {
        /** Wrong id type */
        await request(server)
        .get('/api/v1/posts/5e5')
        .set('Authorization', 'Token ' + token)
        .then((response: request.Response) => {
            expect(response.status).toBe(422);
        });

        /** Get unexisting post */
        await request(server)
        .get('/api/v1/posts/aaaaaaaaaaadd6051857093f')
        .set('Authorization', 'Token ' + token)
        .then((response: request.Response) => {
            expect(response.status).toBe(200);
            expect(response.body).toBeNull;
        });

        /** OK */
        await request(server)
        .get('/api/v1/posts/5e5d4c29578dd6051857093f')
        .set('Authorization', 'Token ' + token)
        .then((response: request.Response) => {
            expect(response.status).toBe(200);
        });
    }, 10000);

});