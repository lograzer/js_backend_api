/**
 * Represent a decoded token
 */
export interface Token {
    /** Id of the user */
    _id: string;

    /** email of the user */
    email: string;

    /** expiration date of the token */
    exp: number;
}