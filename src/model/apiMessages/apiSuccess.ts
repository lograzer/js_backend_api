import { Response } from 'express';
import { ApiMessage } from './apiMessage';

/**
 * Represent an API Success.
 */
export class ApiSuccess implements ApiMessage {
    discriminator: "API-MESSAGE-TYPE" = "API-MESSAGE-TYPE";
    
    /** HTTP success code */
    code: number;

    /** Result to send via the API */
    response: object | undefined | null;

    constructor(codeSuccess: number, response?: object | null) {
        this.code = codeSuccess;
        this.response = response;
    }

    /**
     * Send the succoss to the given Response
     * @param res Response where we want to send the success
     */
    send(res: Response): Response {
        return res.status(this.code).json(this.response);
    }

}