import { Response } from 'express';
import { ApiMessage } from './apiMessage';

/**
 * Represent an API Error.
 * This is an error thrown intentionaly by the API.
 */
export class ApiError extends Error implements ApiMessage {
    discriminator: "API-MESSAGE-TYPE" = "API-MESSAGE-TYPE";
    
    /** code Error */
    code: number;

    constructor(codeError: number, message?: string) {
        super(message);
        this.code = codeError;
        Object.setPrototypeOf(this, ApiError.prototype);
    }

    /**
     * Send the error to the given Response
     * @param res Response where we want to send the error
     */
    send(res: Response): Response {
        return res.status(this.code).json({ message: this.message});
    }
}