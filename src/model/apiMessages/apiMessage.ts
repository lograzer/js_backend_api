import { Response } from "express";

/**
 * Represent a message sended by the API.
 */
export interface ApiMessage {
    discriminator: 'API-MESSAGE-TYPE';

    /** HTTP code related to the message */
    code: number;

    /**
     * Send the message to the given Response
     * @param res Response where we want to send the message
     */
    send(res: Response): Response;
}

/**
 * Check if the given object is a ApiMessage
 * @param object object we want to check the type
 */
export function instanceOfApiMessage(object: any): object is ApiMessage {
    return object.discriminator === 'API-MESSAGE-TYPE';
}