import { Request } from 'express'

export interface ApiRequest extends Request {
    payload: {
        email: string;
        id: string;
        exp: number;
        iat: number;
    }
}