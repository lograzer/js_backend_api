import { ShortUserExport } from "./user";
import { Types } from "mongoose";

/** Represent a post's comment */
export interface Comment {
    /** Id of the comment */
    _id: Types.ObjectId;

    /** Author of the comment */ 
    author: ShortUserExport;

    /** Descriptive message */
    message: String;
}