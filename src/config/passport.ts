import passport from "passport"
import { Strategy, IVerifyOptions } from "passport-local";

import { DatabaseManager } from "../db/databaseManager";
import { UserExport } from "../model/user";

DatabaseManager.getDb().then( (dbm: DatabaseManager) => {
    const Users = dbm.getModel<UserExport>('users');
    passport.use(new Strategy({
        /** field's name to indicate password where to find the user's email in the request*/
        usernameField: 'email',
        /** field's name to indicate password where to find the user's password in the request*/
        passwordField: 'password',
      }, (email, password, done) => {
        Users.findOne({ email }).select('+salt +hash')
          .then((user) => {
            if(!user || !user.validatePassword(password)) {
              return done(null, false, { message: 'email or password invalid' } as IVerifyOptions);
            }
            return done(null, user);
          }).catch(done);
      }));
})

