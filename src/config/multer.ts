import multer, { memoryStorage } from 'multer';
import { ApiError } from '../model/apiMessages/apiError';
import { Storage } from '@google-cloud/storage';

/** Init cloud storage */
const cloudStorage: Storage = new Storage({
    projectId: process.env.PROJECT_ID,
    keyFilename: process.env.KEY_FILE_NAME
});

/** Field where multer will look for the picture */
const PICTURE_FIELD: string = "picture";

/** Allowed file's extension */
const ALLOWED_EXTENSION = ['png', 'jpeg', 'jpg'];

/** Set up the storage configuration of Multer */
const storage = multer({
    storage: memoryStorage(),
    fileFilter: function(req, file, callback) {
        const isAllowedExt = ALLOWED_EXTENSION.includes(file.originalname.split('.')[1].toLowerCase());
        const isAllowedMimeType = file.mimetype.startsWith("image/")
        if (isAllowedExt && isAllowedMimeType) {
            callback(null, true)
        }
        else {
            return callback(new ApiError(400, 'The file must be an image with one of the following extension : ' + ALLOWED_EXTENSION.join(', ')));
        }
    }
});

/** Name of the bucket where are stored our images */
export const bucketName: string = process.env.BUCKET_NAME as string;

/** Bucket where are stored our images */
export const bucket = cloudStorage.bucket(process.env.BUCKET_NAME as string);

/** Method to upload a file (image) */
export const uploadSingle = storage.single(PICTURE_FIELD);