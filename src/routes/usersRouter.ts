import { Router, Request, Response, NextFunction } from 'express';

import { auth } from './auth';
import { UserService } from '../services';
import { uploadSingle } from '../config/multer';
import { ApiMessage, instanceOfApiMessage } from '../model/apiMessages/apiMessage';
import { ApiError } from '../model/apiMessages/apiError';
import { ApiRequest } from '../model/apiRequest';
import { commonError } from '../utils';

export const router: Router = Router();

/** Get a specific user */
router.get('/:id', auth.required, async (req: Request, res: Response) => {
	const result: ApiMessage = await  UserService.getUser(req).catch((data: ApiError) => data);
	if (instanceOfApiMessage(result)) {
		return result.send(res);
	}
	return commonError.send(res);
});

/** Get a specific user and his posts */
router.get('/:id/posts', auth.required, async (req: Request, res: Response) => {
	const result: ApiMessage = await  UserService.getUserPosts(req).catch((data: ApiError) => data);
	if (instanceOfApiMessage(result)) {
		return result.send(res);
	}
	return commonError.send(res);
});

/** Check if the user can connect or not */
router.post('/login', auth.optional, async (req: Request, res: Response, next: NextFunction) => {
	const result: ApiMessage = await UserService.logUser(req).catch((data: ApiError) => data);
	if (instanceOfApiMessage(result)) {
		return result.send(res);
	}
	return commonError.send(res);
});

/** Insert a new user and return his token */
router.post("/", auth.optional, async (req: Request, res: Response) => {
	const result: ApiMessage = await UserService.insertUser(req).catch((data: ApiError) => data);
	if (instanceOfApiMessage(result)) {
		return result.send(res);
	}
	return commonError.send(res);
});

/** Update an existing user */
router.put("/", auth.required, async (req: Request, res: Response) => {
	uploadSingle(req, res, async (err: ApiError) => {
		if (err) { return err.send(res); }
		const result: ApiMessage = await UserService.updateUser(req as ApiRequest).catch((data: ApiError) => data);
		if (instanceOfApiMessage(result)) {
			return result.send(res);
		}
		return commonError.send(res);
	})
});

/** Delete a specific user */
router.delete('/:id', auth.required, async (req: Request, res: Response) => {
	const result: ApiMessage = await  UserService.deleteUser(req as ApiRequest).catch((data: ApiError) => data);
	if (instanceOfApiMessage(result)) {
		return result.send(res);
	}
	return commonError.send(res);
});


