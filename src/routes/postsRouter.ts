import { Router } from 'express';

import { PostService } from '../services';
import { ApiMessage, instanceOfApiMessage } from '../model/apiMessages/apiMessage';
import { ApiError } from '../model/apiMessages/apiError';
import { ApiRequest } from '../model/apiRequest';
import { uploadSingle } from '../config/multer';
import { commonError } from '../utils';

export const router: Router = Router();

/** Add a new post */
router.post("/", async (req, res) => {
	uploadSingle(req, res, async (err: ApiError) => {
		if (err) { return err.send(res); }
		const result: ApiMessage = await PostService.insertPost(req as ApiRequest).catch((data: ApiError | any) => data);
		if (instanceOfApiMessage(result)) {
			return result.send(res);
		}
		return commonError.send(res);
	});
});

/** Update an existing post */
router.put("/", async (req, res) => {
	const result: ApiMessage = await PostService.updatePost(req as ApiRequest).catch((data: ApiError) => data);
	if (instanceOfApiMessage(result)) {
		return result.send(res);
	}
	return commonError.send(res);
});

/** Add a comment */
router.post("/:id/comments", async(req, res) => {
	const result: ApiMessage = await PostService.commentPost(req as ApiRequest).catch((data: ApiError) => data);
	if (instanceOfApiMessage(result)) {
		return result.send(res);
	}
	return commonError.send(res);
})

/** get a post and its comments */
router.get("/:id/comments", async(req, res) => {
	const result: ApiMessage = await PostService.getPostComments(req as ApiRequest).catch((data: ApiError) => data);
	if (instanceOfApiMessage(result)) {
		return result.send(res);
	}
	return commonError.send(res);
})

/** delete a comment */
router.delete("/:id/comments/:idComment", async(req, res) => {
	const result: ApiMessage = await PostService.uncommentPost(req as ApiRequest).catch((data: ApiError) => data);
	if (instanceOfApiMessage(result)) {
		return result.send(res);
	}
	return commonError.send(res);
})

/** Add a like */
router.post("/:id/likes", async(req, res) => {
	const result: ApiMessage = await PostService.likePost(req as ApiRequest).catch((data: ApiError) => data);
	if (instanceOfApiMessage(result)) {
		return result.send(res);
	}
	return commonError.send(res);
})

/** Get likes */
router.get("/:id/likes", async(req, res) => {
	const result: ApiMessage = await PostService.getPostLikes(req as ApiRequest).catch((data: ApiError) => data);
	if (instanceOfApiMessage(result)) {
		return result.send(res);
	}
	return commonError.send(res);
})

/** Delete a like */
router.delete("/:id/likes", async(req, res) => {
	const result: ApiMessage = await PostService.unlikePost(req as ApiRequest).catch((data: ApiError) => data);
	if (instanceOfApiMessage(result)) {
		return result.send(res);
	}
	return commonError.send(res);
})

/** Get posts of the database
 *  Query parameters start and limit are required
 */
router.get('/', async (req, res) => {
	const result: ApiMessage = await PostService.getPosts(req).catch((data: ApiError) => data);
	if (instanceOfApiMessage(result)) {
		return result.send(res);
	}
	return commonError.send(res);
});

/** Get posts randomly from the database
 *  Query parameters limit is required
 */
router.get('/random', async (req, res) => {
	const result: ApiMessage = await PostService.getRandomly(req).catch((data: ApiError) => data);
	if (instanceOfApiMessage(result)) {
		return result.send(res);
	}
	return commonError.send(res);
});

/** Get a specific post */
router.get('/:id', async (req, res) => {
	const result: ApiMessage = await PostService.getPost(req).catch((data: ApiError) => data);
	if (instanceOfApiMessage(result)) {
		return result.send(res);
	}
	return commonError.send(res);
});


/** Delete a specific post */
router.delete('/:id', async (req, res) => {
	const result: ApiMessage = await PostService.deletePost(req as ApiRequest).catch((data: ApiError) => data);
	if (instanceOfApiMessage(result)) {
		return result.send(res);
	}
	return commonError.send(res);
});
