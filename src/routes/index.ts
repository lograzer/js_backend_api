import { Router } from 'express';

import { auth } from './auth';
import { router as usersRouter } from './usersRouter';
import { router as postsRouter } from './postsRouter';

export const router: Router = Router();

// *********** ROUTES *************
router.use("/users", usersRouter);
router.use("/posts", auth.required);
router.use("/posts", postsRouter);
