import jwt from "express-jwt";

/**
 * Get the JWT token sent by the client in the request's headers
 * @param req request where we want to extract the JWT token
 */
export const getTokenFromHeaders: jwt.GetTokenCallback = (req: any): string | null => {
  const authorization = req.headers.authorization;

  if(authorization && authorization.split(' ')[0] === 'Token') {
    return authorization.split(' ')[1];
  }
  return null;
};

/**
 * Auth object with optional and required properties
 */
export const auth = {
  required: jwt({
    secret: process.env.JWT_SECRET as string,
    userProperty: 'payload',
    getToken: getTokenFromHeaders,
  }),
  optional: jwt({
    secret: process.env.JWT_SECRET as string,
    userProperty: 'payload',
    getToken: getTokenFromHeaders,
    credentialsRequired: false,
  })
};
