import { Request } from 'express';
import fs  from 'fs';

import { bucket, bucketName } from '../config/multer';
import { ApiRequest } from '../model/apiRequest';

/**
 * Delete a picture from the file system 
 * @param path Path to the image
 */
export function deletePicture(path: string): void {
    if (path) {
        const imgSplitPath = path.split('/');
        const imgName = imgSplitPath[imgSplitPath.length-1];
        bucket.deleteFiles({
            prefix: imgName
        }, console.log)
    }
}

export function uploadPicture(req: ApiRequest) {
    // Create a new blob in the bucket and upload the file data.
    const name = req.payload.id + '_' + new Date().getTime() + '_' + req.file.originalname;
    const blob = bucket.file(name);
    (req.file as any).name = name;
    const blobStream = blob.createWriteStream();

    blobStream.on('error', console.log);
    blobStream.end(req.file.buffer);
}

/**
 * Build the image htt ppath to get the correponding one to access it with an http request
 * @param req http request
 */
export function buildImagePath(req: Request): string {
    return `https://storage.googleapis.com/${bucketName}/${(req.file as any).name}`;
}