import { Types, Model } from 'mongoose';
import { authenticate } from 'passport';
import { Request } from 'express';

import { DatabaseManager } from '../db/databaseManager';
import { ApiRequest } from '../model/apiRequest';
import { UserExport, UserCandidate, copy } from '../model/user';
import { ApiSuccess } from '../model/apiMessages/apiSuccess';
import { ApiError } from '../model/apiMessages/apiError';
import { ApiMessage } from '../model/apiMessages/apiMessage';
import { PostService } from './postService';
import { buildImagePath, deletePicture, uploadPicture } from './pictureService';
import { getFieldNumber, checkNumberQuery } from '../utils';
import { PostExport } from '../model/post';

let db: DatabaseManager;
let model: Model<UserExport>;

DatabaseManager.getDb().then((dbm: DatabaseManager) => {
	db = dbm;
	model = db.getModel('users');
});


export abstract class UserService {
    /**
     * Insert a new user and return his token
     * @param req http request
     */
    static async insertUser(req: Request): Promise<ApiSuccess> {
        if (!req.body.email || !req.body.password){
            throw new ApiError(422, "Email and password are required.");
        }
        const emailCheck=/^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$/i;
        if (!emailCheck.test(req.body.email)) {
            throw new ApiError(422, "Email hasn't the correct format, please enter a correct email address.");
        }
        if (await model.countDocuments({ email: req.body.email })) {
            throw new ApiError(422, "This email address is already taken. Choose another one.");
        }
        const user: UserCandidate = {
            pseudo: req.body.email.substr(0, req.body.email.indexOf('@')),
            email: req.body.email
        } as UserCandidate;
        const newUser: UserExport = await model.create(user);
        newUser.setPassword(req.body.password.trim());
        newUser.save();
        return new ApiSuccess(201, { token: newUser.toAuth() });
    }

    /**
     * Update an existing user
     * @param req http request
     */
    static async updateUser(req: ApiRequest): Promise<ApiSuccess>  {
        if (!req.body.id || !req.body.pseudo){
            throw new ApiError(422, "id and pseudo are required");
        }
        if (req.body.id !== req.payload.id) { 
            throw new ApiError(403, "You're not allowed to update this ressource.");
        }
        const user: UserExport | null = await model.findById(req.body.id);
        if (!user) {
            throw new ApiError(404, "The user doesn't longer exit");
        }
        if (req.file) {
            deletePicture(user.avatar);
            uploadPicture(req);
            user.avatar = buildImagePath(req);
        }
        user.pseudo = req.body.pseudo;
        user.save();
        PostService.updatePostsAuthor({
            _id: req.body.id,
            avatar: user.avatar,
            pseudo: user.pseudo
        });
        return new ApiSuccess(200);
    }

    /**
     * Check if the user can connect or not
     * @param req http request
     * @param res 
     * @param next 
     */
    static async logUser(req: Request): Promise<ApiMessage> {
        if (!req.body.email || !req.body.password){
            throw new ApiError(422, "Please, provide an email address and a password");
        }

        return new Promise((resolve, reject) => {
            authenticate('local', {session: false}, (err: Error, passportUser: UserExport, info: any) => {
                if(err) {
                    reject(new ApiError(400, "An error has been occured during the user's login."));
                }
                if(passportUser) {
                    const user = passportUser;
                    user.token = passportUser.generateJWT();
                    return resolve(new ApiSuccess(200, { token: user.toAuth() }));
                }
                reject(new ApiError(400, "Email or password incorrect."));
            })(req);
        })
    }

    /**
     * Get a specific user without its posts
     * @param req http request
     */
    static async getUser(req: Request): Promise<ApiSuccess>  {
        let result = { } as UserExport;
        const data: UserExport | null = await model.findById(req.params.id);
        if (data) {
            result = {
                _id: data._id,
                avatar: data.avatar,
                pseudo: data.pseudo,
                email: data.email
            } as UserExport;
            const postsNumberObj = await getFieldNumber(model, "posts", result._id.toHexString());
            if (postsNumberObj) {
                result.postsNumber = postsNumberObj[0]?.number;
            }
        }
        return new ApiSuccess(200, result);
    }

    /**
     * Get a specific user and its posts
     * @param req http request
     */
    static async getUserPosts(req: Request): Promise<ApiSuccess> {
        if (!req.params.id  || !Types.ObjectId.isValid(req.params.id)) {
            throw new ApiError(400, "Please specify the user where you want to retrieve its posts.");
        }
        let limit: number = checkNumberQuery(req, 'limit', 50);
        let start: number = checkNumberQuery(req, 'start');
        const user: UserExport | null = await model.findById(req.params.id);
        if (!user) {
            throw new ApiError(400, "It's not possible to get those likes because the post doesn't longer exist.");
        }
        const userWithPosts: UserExport = await model.findById(req.params.id, { posts: { $slice: [ start, limit ] } }).select('+posts').populate('posts') as UserExport;
        const postsNumberObj = await getFieldNumber(model, "posts", user._id.toHexString());
        for (let postIndex in userWithPosts.posts as unknown as PostExport[]) {
            (userWithPosts.posts as unknown as PostExport[])[postIndex] = await PostService.getElementsNumber((userWithPosts.posts as unknown as PostExport[])[postIndex]);
        }
        if (postsNumberObj) {
            userWithPosts.postsNumber = postsNumberObj[0]?.number;
        }
        return new ApiSuccess(200, copy(userWithPosts));
    }

    /**
     * Delete a specific user
     * @param req http request
     */
    static async deleteUser(req: ApiRequest): Promise<ApiSuccess> {
        if (req.params.id !== req.payload.id) { 
            throw new ApiError(403, "You're not allowed to delete this user.");
        }
        const user: UserExport | null = await model.findById(req.params.id);
        if (!user) { throw new ApiError(400, "The user can't be deleted because he doesn't exist."); }
        deletePicture(user.avatar);
        if (user.posts) {
            for (const post of user.posts) {
                req.params.id = post.toHexString();
                await PostService.deletePost(req);
            }
        }
        await user.remove();
        return new ApiSuccess(200);
    }

    /**
     * Add an existing post to the user's informations
     * @param userId User who own to post
     * @param postId Post to add to the user
     */
    static async addPostUser(userId: Types.ObjectId, postId: Types.ObjectId): Promise<ApiSuccess> {
        const user: UserExport | null = await model.findById(userId);
        if (user) {
            await model.updateOne({_id: userId}, {$push: {posts: postId}});
            return new ApiSuccess(200);
        } else {
            throw new ApiError(400, "The user who own the post doesn't exist.");
        }
    }

    /**
     * Remove an existing post to the user's informations
     * @param userId id of the user who own the document
     * @param postId post we want to remove from his list
     */
    static async deletePostUser(userId: Types.ObjectId, postId: Types.ObjectId): Promise<ApiSuccess> {
        if (!userId  || !Types.ObjectId.isValid(userId)) {
            throw new ApiError(422, "Please specify the owner of the post to delete");
        }
        await model.updateOne({_id: userId}, {$pull: { posts: postId}});
        return new ApiSuccess(200);
    }

    /**
     * Check if the user exists or not in the database
     * @param userId id of the user we want to check
     */
    static async isExisting(userId: Types.ObjectId): Promise<boolean> {
        return model.exists({_id: userId})
    }
}


