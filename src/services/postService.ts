import { Router, Request } from 'express';
import {  Types, Model } from 'mongoose';

import { DatabaseManager } from '../db/databaseManager';
import { PostCandidate, PostExport, copy } from '../model/post';
import { ApiSuccess } from '../model/apiMessages/apiSuccess';
import { ApiError } from '../model/apiMessages/apiError';
import { UserService } from './userService';
import { ShortUserExport, UserExport } from '../model/user';
import { ApiRequest } from '../model/apiRequest';
import { deletePicture, buildImagePath, uploadPicture } from './pictureService';
import { Comment } from '../model/comment';
import { getFieldNumber, checkNumberQuery } from '../utils';

export const router: Router = Router();
let db: DatabaseManager;
let model: Model<PostExport>;

DatabaseManager.getDb().then((dbm: DatabaseManager) => {
	db = dbm;
	model = db.getModel<PostExport>('posts');
});

export abstract class PostService {

    /**
     * Add a new post
     * @param req http request
     */
    static async insertPost(req: ApiRequest): Promise<ApiSuccess> {
        if (!req.file) {
            throw new ApiError(422, "An image is required.")
        }
        if (!req.body.title){
            deletePicture(buildImagePath(req));
            throw new ApiError(422, "A title is required.");
        }
        req.params.id = req.payload.id
        uploadPicture(req);
        const post: PostCandidate = await UserService.getUser(req).then(async (apiSuccess: ApiSuccess): Promise<PostCandidate> => {
            const author: UserExport = apiSuccess.response as UserExport
            if (!author) {
                deletePicture(buildImagePath(req));
                throw new ApiError(404, "The user who wants to create the post doesn't exist.");
            }
            const post: PostCandidate = {
                title: req.body.title,
                description: req.body.description,
                picture: buildImagePath(req),
                author: {
                    _id: new Types.ObjectId(req.payload.id),
                    avatar: author.avatar,
                    pseudo: author.pseudo
                },
                comments: [] as Comment[],
                likes: [] as ShortUserExport[]
            } as PostCandidate;
            const createdPost: PostExport = await model.create(post);
            await UserService.addPostUser(post.author._id, createdPost._id);
            return post;
        }) as PostCandidate;
        return new ApiSuccess(201, post);
    }

    /**
     * Update an existing post
     * @param req http request
     */
    static async updatePost(req: ApiRequest): Promise<ApiSuccess> {
        if (!req.body.id || !Types.ObjectId.isValid(req.body.id)){
            throw new ApiError(422, "Please identify your post with the id section.");
        }
        if (!req.body.title){
            throw new ApiError(422, "Title is required");
        }
        const post: PostExport | null = await model.findById(req.body.id);
        if (!post) {
            throw new ApiError(400, "The post doesn't longer exit");
        }
        if (post.author._id.toHexString() !== req.payload.id) {
            throw new ApiError(403, "You're not allowed to update this ressource.");
        }
        post.title = req.body.title;
        post.description = req.body.description;
        post.save();
        return new ApiSuccess(200);
    }

    /**
     * Add a comment to a post
     * @param req http request
     */
    static async commentPost(req: ApiRequest): Promise<ApiSuccess> {
        let post: PostExport | null;
        if (!req.params.id || !Types.ObjectId.isValid(req.params.id)) {
            throw new ApiError(422, "Please specify the post to comment.");
        }
        if (!req.body.message || (req.body.message as string).trim().length === 0) {
            throw new ApiError(422, "Please specify the message to post.");
        }
        post = await model.findById(req.params.id);
        if (!post) {
            throw new ApiError(400, "It's not possible to comment this post because the post doesn't longer exist.");
        }
        req.params.id = req.payload.id
        const comment: Comment = await UserService.getUser(req).then(async (apiSuccess: ApiSuccess): Promise<Comment> => {
            const author: UserExport = apiSuccess.response as UserExport
            if (!author) {
                throw new ApiError(400, "The user who wants to comment the post doesn't exist.");
            }
            return {
                _id: new Types.ObjectId(),
                author: {
                    _id: new Types.ObjectId(req.payload.id),
                    avatar: author.avatar,
                    pseudo: author.pseudo
                },
                message: req.body.message
            } as Comment;
        });
        await model.updateOne({_id: post._id}, { $push: { comments: comment} });
        return new ApiSuccess(200);
    }
    
    /**
     * Remove a comment from a post
     * The comment can be remove only if this is the owner of the post or the owner of the comment who does the action
     * @param req http request
     */
    static async uncommentPost(req: ApiRequest): Promise<ApiSuccess> {
        if (!req.params.id || !req.params.idComment || !Types.ObjectId.isValid(req.params.id) || !Types.ObjectId.isValid(req.params.idComment)) {
            throw new ApiError(422, "Please specify the post to uncomment and the related comment.");
        }
        let post: PostExport |undefined = undefined;
        const posts: PostExport[] = (await model.aggregate(
            [
                { $unwind: "$comments" },
                { $match: {"_id": new Types.ObjectId(req.params.id) , "comments._id": new Types.ObjectId(req.params.idComment)} }
            ]
        ));
        if (posts) {
            post = posts[0];
        } 
        if (!post) {
            throw new ApiError(400, "It's not possible to uncomment this post because the post or the comment doesn't exist anymore.");
        }
        if (post.author._id.toHexString() !== req.payload.id && (post.comments as unknown as Comment).author._id.toHexString() !== req.payload.id) {
            throw new ApiError(403, "You're not allowed to delete this ressource");
        }
        await model.updateOne({_id: post._id}, {$pull: { comments: { _id: new Types.ObjectId(req.params.idComment)}}})
        return new ApiSuccess(200);
    }

    /**
     * Get comments of a post depending on the start and limit given query parameters
     * When we get post's comment, it updates the author reference too in case their is some update on its profile
     * @param req http request
     */
    static async getPostComments(req: ApiRequest): Promise<ApiSuccess> {
        if (!req.params.id  || !Types.ObjectId.isValid(req.params.id)) {
            throw new ApiError(422, "Please specify the post where you want to retrieve its likes.");
        }
        let limit: number = checkNumberQuery(req, 'limit', 50);
        let start: number = checkNumberQuery(req, 'start');
        const post: PostExport | null = await model.findById(req.params.id);
        if (!post) {
            throw new ApiError(400, "It's not possible to get those comments because the post doesn't longer exist.");
        }
        const postWithComments: PostExport = await model.findById(req.params.id, { comments: { $slice: [ start, limit ] } }).select('+comments') as PostExport;
        Promise.all(postWithComments.comments.map(async (comment: Comment) => {
            const commentAuthor: UserExport | null = (await UserService.getUser({ params: { id: comment.author._id.toHexString() }} as unknown as Request)).response as UserExport | null;
            if (commentAuthor) {
                comment.author.avatar = commentAuthor.avatar;
                comment.author.pseudo = commentAuthor.pseudo;
            }
        })).then(() => { postWithComments.save(); })
        .catch(console.log);
        return new ApiSuccess(200, await this.getElementsNumber(postWithComments));
    }


    /**
     * Add a like to a post
     * A like can't be add if the post is already liked by the user
     * @param req http request
     */
    static async likePost(req: ApiRequest): Promise<ApiSuccess> {
        if (!req.params.id || !Types.ObjectId.isValid(req.params.id)) {
            throw new ApiError(422, "Please specify the post to like.");
        }
        const id: Types.ObjectId = new Types.ObjectId(req.params.id);
        const post: PostExport | null = await model.findOne(
            {   
                $and: [
                    { _id: id }, 
                    { likes: { $elemMatch: { _id: new Types.ObjectId(req.payload.id) } } }
                ]
            });
        if (post) {
            throw new ApiError(400, "You can't like this post again because you already liked it.");
        }
        req.params.id = req.payload.id;
        const like: ShortUserExport = await UserService.getUser(req).then(async (apiSuccess: ApiSuccess): Promise<ShortUserExport> => {
            const user: UserExport = apiSuccess.response as UserExport;
            if (!user) {
                throw new ApiError(400, "The user who wants to like the post doesn't exist.");
            }
            return {
                _id: new Types.ObjectId(req.payload.id),
                avatar: user.avatar,
                pseudo: user.pseudo
            } as ShortUserExport;
        });
        await model.updateOne({_id: id}, { $push: { likes: like } });
        return new ApiSuccess(200);
    }

    /**
     * Remove a like from a post
     * @param req http request
     */
    static async unlikePost(req: ApiRequest): Promise<ApiSuccess> {
        if (!req.params.id  || !Types.ObjectId.isValid(req.params.id)) {
            throw new ApiError(422, "Please specify the post to unlike");
        }
        const idUser: Types.ObjectId = new Types.ObjectId(req.payload.id);
        const post: PostExport = (await model.aggregate(
            [
                { $unwind: "$likes" },
                { $match: {"_id": new Types.ObjectId(req.params.id) , "likes._id": idUser} }
            ]
        ))[0];
        if (!post) {
            throw new ApiError(400, "You can't unlike this post again because you need to like it first.");
        }
        await model.updateOne({_id: post._id}, {$pull: { likes: { _id: idUser}}})
        return new ApiSuccess(200);
    }

    /**
     * Get a post's likes
     * @param req 
     */
    static async getPostLikes(req: ApiRequest): Promise<ApiSuccess> {
        if (!req.params.id  || !Types.ObjectId.isValid(req.params.id)) {
            throw new ApiError(422, "Please specify the post where you want to retrieve its likes.");
        }
        let limit: number = checkNumberQuery(req, 'limit', 50);
        let start: number = checkNumberQuery(req, 'start');
        const post: PostExport | null = await model.findById(req.params.id);
        if (!post) {
            throw new ApiError(400, "It's not possible to get those likes because the post doesn't longer exist.");
        }
        const postWithLikes: PostExport = await model.findById(req.params.id, { likes: { $slice: [ start, limit ] } }).select('+likes') as PostExport;
        Promise.all(postWithLikes.likes.map(async (like: ShortUserExport): Promise<void> => {
            const likeAuthor: UserExport | null = (await UserService.getUser({ params: { id: like._id.toHexString()}} as unknown as Request)).response as UserExport | null;
            if (likeAuthor) {
                like.avatar = likeAuthor.avatar;
                like.pseudo = likeAuthor.pseudo;
            }
        })).then(() => { postWithLikes.save() })
        .catch(e => { console.log(e); throw e; });
        return new ApiSuccess(200, await this.getElementsNumber(postWithLikes));
    }

    /**
     * Get posts of the database, you can't get more than 50 post by call
     * Query parameters start and limit are required
     * @param req http request
     */
    static async getPosts(req: Request): Promise<ApiSuccess> {
        let limit: number = checkNumberQuery(req, 'limit', 50);
        let start: number = checkNumberQuery(req, 'start');
        let posts = await model.find({}).skip(start).limit(limit);
        posts = await Promise.all(posts.map(async (val: PostExport) => this.getElementsNumber(val)));
        return new ApiSuccess(200, posts);
    }

    /**
     * Get posts randomly from the database
     * You can't get more than 50 posts by call
     * Query parameters limit is required
     * @param req http request
     */
    static async getRandomly (req: Request): Promise<ApiSuccess> {
        let limit: number = checkNumberQuery(req, 'limit', 50);
        let done: number[] = [];
        let numberDocuments: number =  await model.countDocuments();
        for (let i=0; i<limit && i<numberDocuments; ++i) { 
            done.push(this.randomExcluded(numberDocuments, done));
        }
        let posts: PostExport[] = await Promise.all(
            done.map(async (value: number) => {
                const post: PostExport[] = await model.find({}).skip(value).limit(1) as unknown as PostExport[];
                return this.getElementsNumber(post[0]);
            }));
        return new ApiSuccess(200, posts);
    }

    /**
     * generate a random number between 0 and the given max number.
     * This function excluds the number given in the param excluded
     * @param max num max we want to generate. This number is excluded
     * @param excluded number we don't want to be generated
     */
    private static randomExcluded(max: number, excluded: number[]) {
        let n = Math.floor(Math.random() * max);
        while (excluded.includes(n)) { n = (n + 1)%max;}
        return n;
    }

    /**
     * Get a specific post
     * @param req http request
     */
    static async getPost(req: Request): Promise<ApiSuccess> {
        if (!Types.ObjectId.isValid(req.params.id)) {
           throw new ApiError(422, "Please specify the post to get");
        }
        let post = await model.findById(new Types.ObjectId(req.params.id));
        return new ApiSuccess(200, post ? await this.getElementsNumber(post) : null);
    }
    
    /**
     * Get the number of comments and likes for a post
     * @param post post where we want to retrieve like's and comment's number
     */
    static async getElementsNumber(post: PostExport): Promise<PostExport> {
        if (post) {
            const commentsNumberObj = await getFieldNumber(model, "comments", post._id.toHexString());
            const likesNumberObj = await getFieldNumber(model, "likes", post._id.toHexString());
            if (commentsNumberObj) {
                post.commentsNumber = commentsNumberObj[0]?.number;
            }
            if (likesNumberObj) {
                post.likesNumber = likesNumberObj[0]?.number;
            }
       }
       return copy(post);
    }

    /**
     * Delete a specific post and its author's reference
     * @param req http request
     */
    static async deletePost(req: ApiRequest): Promise<ApiSuccess> {
        if (!Types.ObjectId.isValid(req.params.id)) {
            throw new ApiError(422, "Please specify the post to delete");
         }
        const post: PostExport | null = await model.findById(req.params.id);
        if (post) {
            if (post.author._id.toHexString() !== req.payload.id) { 
                throw new ApiError(403, "You're not allowed to delete this post.");
            }
            deletePicture(post.picture);
            await post.remove();
            return await UserService.deletePostUser(new Types.ObjectId(req.payload.id), new Types.ObjectId(req.params.id));            
        } else {
            throw new ApiError(400, "The given post does not exist, therefore this is impossible to delete it.");
        }
    }

    /**
     * Update the post's authors data
     * @param author new author values
     */
    static async updatePostsAuthor(author: ShortUserExport) {
        const posts: PostExport[] = await model.find({'author._id' : author._id });
        posts?.forEach((post: PostExport) => {
            post.author = author;
            post.save();
        })
    }

    
}



