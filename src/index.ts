import express from 'express';
import { json, urlencoded } from 'body-parser';
import jwt from "jsonwebtoken";
import cors from 'cors';

import './config/passport';
import { router } from './routes';
import { getTokenFromHeaders } from './routes/auth';

export const app = express();

app.use(cors());
app.use(json());
app.use(urlencoded({ extended: false }));

/** decode the token, insert it in the request and continu the process */
app.use(function(req, res, next) {
  const token: string | null = getTokenFromHeaders(req);
  if (token) {
    jwt.verify(token, process.env.JWT_SECRET as string, function(err: Error, decodedToken: any) {
      if(!err) {
        if (new Date().getTime() / 1000 > decodedToken.exp) {
          return res.status(401).json( { message: "The token is no longer valid." });
        }
        next();
      } else {
        return res.status(401).json( { message: "invalid token." });
      }
    });
  } else {
    next();
  }
});

app.use("/api/v1", router);





