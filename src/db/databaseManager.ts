import { Document, connection, connect, Mongoose, model } from 'mongoose'

import { postSchema } from './schemas/postSchema'
import { userSchema } from './schemas/userSchema'

export class DatabaseManager {
    /** Unique instance of the database manager */
    private static _db: DatabaseManager;

    private constructor() {
        model('posts', postSchema);
        model('users', userSchema).createIndexes();
    }

    /** Get the unique instance of the database manager */
    public static async getDb(): Promise<DatabaseManager> {
        if (this._db) {
            return this._db;
        } 
        this._db = new DatabaseManager();
        await this._db.connect();
        return this._db;
    }

    /** Connect the database */
    private connect(): Promise<void | Mongoose> {
        return connect(process.env.DATA_BASE_URL as string, 
            {useNewUrlParser: true, useUnifiedTopology: true, useFindAndModify: false}).catch(console.error);
    }

    /**
     * Get a specific model
     * @param name model's name
     */
    getModel<T extends Document>(name: string) {
        return connection.model<T>(name);
    }
}