import { Schema, Types } from "mongoose";

export let postSchema = new Schema({
    title: String,
    picture: String,
    description: String,
    comments: 
        {
            type: [{
                    _id: Types.ObjectId,
                    author: {
                        pseudo: String,
                        avatar: String,
                        _id: {type: Types.ObjectId, ref:'users'}
                    },
                    message: String
                }],
            select: false
        },
    likes: 
        {
            type: [{
            pseudo: String,
            avatar: String,
            _id: {type: Types.ObjectId, ref:'users'}
            }],
            select: false
        },
    author: {
        pseudo: String,
        avatar: String,
        _id: {type: Types.ObjectId, ref:'users'}
    }
});