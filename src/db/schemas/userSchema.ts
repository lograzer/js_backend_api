import { Schema, Types } from "mongoose";
import crypto from "crypto";
import jwt from "jsonwebtoken";

/** definition of the user's schema */
export let userSchema = new Schema({
  /** pseudo of the user */
  pseudo: String,

  /** path to retrieve the user's avatar*/
  avatar: String,

  /** email of the user */
  email: {
      type: String,
      unique: true,
      index: true
  },

  /** posts owned by the user */
  posts: {
    type: [{
      type: Types.ObjectId,
      ref:'posts'}
    ],
    select: false
  },

  /** hash of the user's passwor */
  hash: {
    type: String,
    select: false
  },

  /** salt to makes the password's hash stronger */
  salt: {
    type: String,
    select: false
  }
});

/** creation of an index in the user's schema */
userSchema.index({email: 1});

/** function to update the user's password */
userSchema.methods.setPassword = function(password: string) {
  this.salt = crypto.randomBytes(16).toString('hex');
  this.hash = crypto.pbkdf2Sync(password, this.salt, 10000, 512, 'sha512').toString('hex');
}

/** function to know if the password is valid */
userSchema.methods.validatePassword = function(password: string): boolean {
  const hash = crypto.pbkdf2Sync(password, this.salt, 10000, 512, 'sha512').toString('hex');
  return this.hash === hash;
};

/** function to generate the JWT token */
userSchema.methods.generateJWT = function(): string {
  const today = new Date();
  const expirationDate = new Date(today);
  expirationDate.setDate(today.getDate() + 1);
  return jwt.sign({
    email: this.email,
    id: this._id,
    exp: parseInt((expirationDate.getTime() / 1000).toString(), 10),
  }, process.env.JWT_SECRET as string);
}
  
/** function to retrieve the user's auth data */
userSchema.methods.toAuth = function(): string {
  return this.generateJWT();
};
