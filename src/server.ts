import dotenv from "dotenv";

dotenv.config();

import { app } from ".";

const server = app.listen(process.env.PORT, () => {
    console.log(`server running on port ${process.env.PORT}`);
});

module.exports = server;
