import { Model, Document, Types } from "mongoose";

/**
 * Count the number of element in an object, for a specific post
 * @param fieldName Name of the field we want to count
 * @param id id of the post where we want to count a field
 */
export async function getFieldNumber<T extends Document>(model: Model<T>, fieldName: string, id: string): Promise<any> {
    return model.aggregate([{$match: {'_id': new Types.ObjectId(id)}}, {$unwind : "$" + fieldName }, {$count: "number"}]);
}
