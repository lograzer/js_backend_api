import { Request } from 'express'

import { ApiError } from '../model/apiMessages/apiError';
import { ApiMessage } from '../model/apiMessages/apiMessage';

export const commonError: ApiMessage = new ApiError(400, 'An error occured. Please contact an administrator.');

/**
 * Check if the start and limit 
 * @param req 
 * @param paramName 
 */
export function checkNumberQuery(req: Request, paramName: string, maxValue?: number): number {
    const param = req.query[paramName];
    if (!param) { 
        throw new ApiError(422, `Query parameters are missing. Parameters ${paramName} must be given in the URL address.`);
    }
    let paramNumber: number = parseInt(param, 10);
    if (isNaN(paramNumber)) { 
        throw new ApiError(422, `Query parameter ${paramName} must be number.`);
    }
    if (maxValue && paramNumber > maxValue) {
        paramNumber = maxValue;
    }
    if (paramNumber < 0) { paramNumber = 0; }
    return paramNumber;
}